﻿#NoEnv  								; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  								; Enable warnings to assist with detecting common errors.
SendMode Input  						; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  			; Ensures a consistent starting directory.


;Created on 18.07.27 by Loeffner


;When pressing CapsLock + (a,o,u) this prints the corresponding german umlaut (ä,ö,ü).
;Pause or unpause the script with Alt+CapsLock



Alt & CapsLock::Suspend 				; Pressing Alt and CapsLock pauses the script

SetCapsLockState, AlwaysOff				; Turn off CapsLock

CapsLock & a:: Send ä
CapsLock & o:: Send ö
CapsLock & u:: Send ü 
CapsLock & s:: Send ß
::Ae::Ä
::Oe::Ö
::Ue::Ü 

;Latex Versions
RCtrl & a:: Send {{}{\}{"}a{}}
RCtrl & o:: Send {{}{\}{"}o{}}
RCtrl & u:: Send {{}{\}{"}u{}}
RCtrl & s:: Send {{}{\}ss{}}


;Capital Letters:
	;I could not find a way to get "CapsLock & Shift & A::Send, Ä" to work
	;So I chose to implement it with hotstrings